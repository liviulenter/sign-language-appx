/**
 * the app comunicates with the local Json server DB using https as the comunication protocol. 
 * It is very important to remeber to start the JSON DB SERVER before you start the app!
 */
//Verifys if the user exists in the db
export const isUserInDatabase = async (user) => {
    const USER_URL = "http://localhost:3010/users?name=" + user;
    const response = await fetch(USER_URL);
    const names = await response.json();
    if (names.length === 0) {
        return false;
    }
    return true;
}

//Adds the user to the db
export const addUser = async (name) => {
    await fetch('http://localhost:3010/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: name
        })
    })
}
//Add the new translation to db
export const addTranslation = async (body) => {
    await fetch('http://localhost:3010/translations', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
}

//Delete the translation from db
export const deleteTranslation = async (translation) => {
    translation.status = "deleted";
    const response = await fetch("http://localhost:3010/translations/" + translation.id, {
        method: 'PATCH',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(translation)
    })
    const data = await response.json()
    return data;
}

//Get the most recent translations  for the specified user with the status active (that are not deleted)
export const getUsersMostRecentTranslations = async (user) => {
    const response = await fetch("http://localhost:3010/translations?_sort=id&_order=desc&_limit=10&status=active&author=" + user)
    const data = await response.json();
    return data;
}