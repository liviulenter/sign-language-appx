import { Nav, Navbar } from "react-bootstrap";
import { getStorage } from "../utils/storage";
import AppContainer from "./AppContainer";
/**
 * The navigation bar that is displayed on every page of the app
 */
const NavBar = ({ children }) => {
    const user = getStorage('name');
    //background color of the navbar
    const bgNav = {backgroundColor: '#CDB4DB'};
    return (
        <>
            <Navbar collapseOnSelect fixed='top'  style={bgNav}>
                <AppContainer>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id='responsive-navbar-nav'>
                        <Nav>
                            <Nav.Link href="/translation">Translate</Nav.Link>
                            <Nav.Link href="/profile">{user}</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </AppContainer>
            </Navbar>
        </>
        
    )
}
export default NavBar;